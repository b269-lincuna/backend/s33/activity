//s22 - map
//https://jsonplaceholder.typicode.com/todos
//--------------------------------------

// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/")
.then((response) => response.json())

// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
.then((json) => console.log(json.map(jsonTitle => jsonTitle.title)));

/* LONG CODE
.then((json) => {
	// json.map(jsonTitle => console.log(`${jsonTitle.id}: ${jsonTitle.title}`));
	let arrJson = json.map(jsonTitle => jsonTitle.title);
	console.log(arrJson);
});
*/

//--------------------------------------

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "GET",
	headers: {"Content-Type": "application/json"}
})
.then((response) => response.json())
.then((json) => {
	console.log(json);

//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

	console.log(`The item "${json.title}" on the list has a status of ${json.completed}`);
});

//--------------------------------------


//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

// Creating a post 

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//---------------------

//8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description:"To update the my to do list with a different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//---------------------

// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

 // FOR CHECKING
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({

// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

		dateCompleted: "07/09/21",
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//---------------------

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE",
})
.then((response) => response.json());

